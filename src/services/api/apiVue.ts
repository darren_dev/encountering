import ApiService from './apiService'

// This exports the plugin object.
export default {
  // The install method will be called with the Vue constructor as
  // the first argument, along with possible options
  install(Vue: any) {
    Vue.prototype.$apiService = new ApiService()
    // Vue.prototype.$firebaseService,
  }
}
