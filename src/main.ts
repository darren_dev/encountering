import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import ApiService from '@/services/api/apiVue'
import FirebaseService from '@/services/firebase/firebaseVue'

Vue.config.productionTip = false

const vueInstanceWithFirebase = Vue.use(FirebaseService) // Put FirebaseService before the other Services, in case the other Services rely on Firebase
const firebaseService = vueInstanceWithFirebase.prototype.$firebaseService

Vue.use(ApiService)

router.beforeEach(async (to, from, next) => {
  const loggedIn = await firebaseService.loggedIn()
  if (to.meta.secure) {
    if (loggedIn) {
      next()
    } else {
      next({ name: 'login' })
    }
  }

  next()
})

let vueApp
firebaseService.auth.onAuthStateChanged((t) => {
  store.commit('authorized', t !== null)
  if (!vueApp) {
    vueApp = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})
