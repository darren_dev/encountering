import { Module, ActionTree, MutationTree, GetterTree } from 'vuex'
import { RootState, BasicState } from '../states'

export const state: BasicState = {
  basicItem: {}
}

export const actions: ActionTree<BasicState, RootState> = {
  reset({state}) {
    state.basicItem = {}
  }
}

export const mutations: MutationTree<BasicState> = {
  basicItem: (state, basicItem) => (state.basicItem = basicItem)
}

export const getters: GetterTree<BasicState, RootState> = {
  basicItem: state => state.basicItem,
}

const namespaced = true

export const basicModule: Module<BasicState, RootState> = {
  namespaced,
  state,
  actions,
  getters,
  mutations
}
