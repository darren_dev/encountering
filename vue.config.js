const webpack = require('webpack')

module.exports = {
  productionSourceMap: false,
  configureWebpack: {
    devtool: 'source-map',
    output: {
      filename:
        process.env.NODE_ENV === 'production' ||
        process.env.NODE_ENV === 'staging'
          ? '[name].[contenthash].js'
          : '[name].js'
    },
    plugins: [new webpack.EnvironmentPlugin(['NODE_ENV'])]
  },
  transpileDependencies: ['vuetify']
}
